// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
/**
 * @param {vscode.ExtensionContext} context
 */


// tableau listant les connexions actives
let listeConnexions=[]
var color1 = "#00FF00";

// blocage permettant de trigger l'ajout si et seulement si on est à distance
// comme un mutex
let blocageAjout=0
let blocageSuppression=0
//let blocageRemplacement=0

// variable permettant de ne mettre à jour que son propre curseur
let blocage=0



function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "codit" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json

	let disposable2 = vscode.commands.registerCommand('extension.codit.deco', function () {
		if (listeConnexions.length==0){
			vscode.window.showErrorMessage('Erreur : Aucune connexion ouverte !')
		  }
		  else{
			listeConnexions[0].close();
			listeConnexions.pop();
			vscode.window.showInformationMessage('Déconnexion au serveur réussie !')
		  }
	})


	

	let disposable = vscode.commands.registerCommand('extension.codit', function () {

		// The code you place here will be executed every time your command is executed
		// current editor

		const
        	io = require("socket.io-client"),
			ioClient = io.connect("http://127.0.0.1:8000");


		// à la réception d'un message d'erreur (si le serveur n'existe pas/n'est pas dispo)
		ioClient.io.on("connect_error", (msg) => {
			vscode.window.showErrorMessage('Serveur introuvable')
		})

		// à la réussite de la connexion avec le serveur
		ioClient.on("connect",  function(socket){
			console.log("Connexion au serveur réussie !")
			vscode.window.showInformationMessage('Connexion au serveur réussie !')
			//ioClient.emit("join","")
		})

		listeConnexions.push(ioClient);

		if(vscode.window.activeTextEditor){
			const editor = vscode.window.activeTextEditor;
			console.log('file found!');
			
			// vérifie si aucune sélection n'est en cours
			if (editor.selection.isEmpty) {
				// the Position object gives you the line and character where the cursor is
			
				// dès que l'utilisateur change la position de son curseur, ce dernier est envoyé au serveur
				vscode.window.onDidChangeTextEditorSelection(() => {
					if (blocage==0){
						const position = editor.selection.active;
						//console.log('curseur à la ligne ' + position.line + ' caractère ' + position.character + ' !');
						
						// envoi de la position du curseur au serveur par la socket
						ioClient.emit("update", { "cursor": {"line": position.line, "column": position.character}})
					}
				})

			}				

		// dès que le texte en cours a une modification, on envoie la modification
		vscode.workspace.onDidChangeTextDocument(changeEvent => {
			for (const change of changeEvent.contentChanges) {

				let tailleSuppression = change.range.end.character - change.range.start.character
				let suppressionCondition2 = change.text==""
				//console.log(change.text)
				let suppressionCaractereCondition = tailleSuppression == 1
		
				let ajoutCondition = change.text!=""
				
		
				// s'il s'agit d'une suppresion d'un bloc de caractère, on envoit l'ordre associé au serveur
				if (suppressionCondition2){
					//console.log("suppression!!")
					if (blocageSuppression == 0){
						//console.log(change.text)
						ioClient.emit("update", { "operation": {"type": "delete", "range": { "start":{"line": change.range.start.line, "column": change.range.start.character},"end":{"line": change.range.end.line, "column": change.range.end.character}}}})
					}
				}
		
				// si on insert du texte
				if (ajoutCondition && !suppressionCondition2){
					//console.log("insertion de texte")
					//console.log(change.range.start.line)
					//console.log(change.range.start.character)
					if (blocageAjout == 0){
						ioClient.emit("update", { "operation": {"type": "insert_char", "position": {"line": change.range.start.line, "column": change.range.start.character}, "text": change.text}})
					}
				}
	
			}
		})


		// type de décoration pour le curseur
		const smallNumberDecorationType = vscode.window.createTextEditorDecorationType({
			borderWidth: '1px',
			borderStyle: 'solid',
			overviewRulerColor: color1,
			overviewRulerLane: vscode.OverviewRulerLane.Right,
			light: {
				// this color will be used in light color themes
				borderColor: color1
			},
			dark: {
				// this color will be used in dark color themes
				borderColor: color1
			}
		});


		ioClient.on("update", (msg) => {

			//console.log(msg)
	
			//s'il s'agit d'un changement de position de curseur
			// Changement de la position du curseur de l'autre personne dès que le serveur
			// envoie l'information
			if (msg.cursor != undefined){
	
				// définition des coordonnées du curseur de l'autre utilisateur
				let coordonneeLigne=parseInt(msg.cursor.line)
				let coordonneeColonne=parseInt(msg.cursor.column)
	
				// changement de position du marqueur sur lequel est placé le 2eme curseur
				var newPosition2 = new vscode.Position(coordonneeLigne,coordonneeColonne);
				let newRange = new vscode.Range(newPosition2,newPosition2);
				vscode.window.activeTextEditor.setDecorations(smallNumberDecorationType, [newRange]);
			}
	
			// s'il s'agit d'une opération qui va modifier le texte
			if (msg.operation != undefined){
	
				// insertion d'un caractère/un bloc de caractères
				if (msg.operation.type=="insert_char"){
					//console.log("insertion de caractère")

					// mise à 1 du blocage, pour éviter d'envoyer un changement de position de curseur
					// au serveur
					blocage=1
					blocageAjout=1
					//console.log(msg.operation.position.line)
					//console.log(msg.operation.position.column)

					// changement de position du curseur pour réaliser l'ajout de caractère
					let newPosition = editor.selection.active.with(msg.operation.position.line, msg.operation.position.column);
					//let newSelection = new vscode.Selection(newPosition, newPosition);
					//editor.selection=newSelection;

					// insertion du caractère
					vscode.window.activeTextEditor.edit( edit => {
						edit.insert(newPosition, msg.operation.text) // because I was scheduled first
					}).then(() => {blocageAjout=0})

					blocage=0
				
				}
	
				// suppression d'un bloc de texte
				if (msg.operation.type=="delete"){

					blocage=1

					//console.log("suppression de caractère")

					blocageSuppression = 1
					
					// changement de position du curseur pour réaliser l'ajout de caractère
					let newPosition2 = editor.selection.active.with(msg.operation.range.start.line, msg.operation.range.start.column);
					let newPosition3 = editor.selection.active.with(msg.operation.range.end.line, msg.operation.range.end.column);
					let newSelection2 = new vscode.Selection(newPosition2, newPosition3);

					vscode.window.activeTextEditor.edit( edit => {
						edit.delete(newSelection2)
					}).then(() => {blocageSuppression=0})

					blocage=0
				}
	
			}
			});


		}else{
			console.log('No active file!');
		}
		// Display a message box to the user
		vscode.window.showInformationMessage('Codit en fonction, prêt pour de l\'édition collaborative!');
	});
	context.subscriptions.push(disposable2);
	context.subscriptions.push(disposable);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
