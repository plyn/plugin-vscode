# Plugin d'édition collaborative inter IDE

Le plugin suivant, destiné à être installé sur l'IDE VSCode, permet l'édition collaborative à plusieurs sur le même fichier de code.

## Pré-réquis

Installation de la nodejs et npm :  
Debian
```
apt install nodejs
apt install npm
```
RHEL CentOS
```
yum install nodejs
yum install npm
```


Installation de la bibliothèque socket.io : 

```
npm install socket.io
```

## installation

Installation, mettre le dossier codit dans les répertoires suivant :  
Linux
```
~/.vscode/extensions/
```
Windows
```
c:/users/<nom_utilisateur>/.vscode/extensions/
```
Dans le fichier *extension.js* modifier l'adresse IP pour mettre l'adresse su serveur à la ligne suivante (à la place de localhost) :
```
ioClient = io.connect("http://localhost:8000");
```
## Lancement
Le serveur doit être lancer au préalable, dans VSCode ouvrir un fichier et ouvrir l'invite de commande Ctrl+Maj+P et lancer la commande **>codit**
## Licence



